# UDP client-server communication

## Server viewpoint:

1. The UDP server is waiting on port 8080 for an image or file from the client. 
2. When the file arrives, it saves it to the "store" directory

## Client viewpoint:

1. The client sends an image or any other file to the server using UDP on port 8080.

# Usage: 

1. Start the server program.
2. Start the client program.
3. The client program will list the files from the "files" directory and will ask the user for a filename input. Enter the name of the file to send to the server.
4. The server responds if the sending was successful or not.