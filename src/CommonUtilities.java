import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class CommonUtilities {
	/**
	 * Returns the summation of a byte array by converting each byte to an integer
	 * and adding it to the sum.
	 * 
	 * @param array the given byte array
	 * @return integer value which is the summation of the byte array
	 */
	static int getSumOfByteArray(byte[] array) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += (array[i] & 0xFF);
		}
		return sum;
	}

	/**
	 * Returns a list of file names which is accessible from the given path.
	 * 
	 * @param storageLocation is the location where the files should be listed.
	 * @return list of file Strings or null if there was an error during the
	 *         process.
	 */
	static List<String> getFilesFromStorage(String storageLocation) {
		try {
			return Files.walk(Paths.get(storageLocation))
					.filter(Files::isRegularFile)
					.map(file -> file.getFileName().toString())
					.collect(Collectors.toList());

		} catch (IOException e) {
			System.err.println("An error occurred while listing the files");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Returns true if the directory exists, false otherwise.
	 * 
	 * @param directoryName
	 * @return boolean, true if the directory exists, false otherwise.
	 */
	static boolean isDirectoryExists(String directoryName) {
		return new File(directoryName).exists();
	}

	/**
	 * Checks if the directory with the given name exists, creates it if not.
	 * 
	 * @param directoryName name of the directory that will be checked.
	 */
	static void checkForDirectory(String directoryName) {
		if (!isDirectoryExists(directoryName)) {
			new File(directoryName).mkdir();
		}
	}
}
