import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Server {

	public static void main(String[] args) throws Exception {
		DatagramSocket serverSocket = new DatagramSocket(8080);

		byte[] bytesReceived = new byte[65504];

		DatagramPacket receivePacket = new DatagramPacket(bytesReceived, bytesReceived.length);

		// Waiting for the data to receive, at port 8080
		serverSocket.receive(receivePacket);

		// Data
		byte[] data = receivePacket.getData();
		byte[] receivedData = Arrays.copyOfRange(data, 0, (data.length - 4));
		byte[] checksumBytes = Arrays.copyOfRange(data, (data.length - 4),
				(data.length));

		if (new BigInteger(checksumBytes).intValue() == CommonUtilities.getSumOfByteArray(receivedData)) {
			System.out.println("Data validation was successful");
			System.out.println("Writing data to store...");
			CommonUtilities.checkForDirectory("store");
			Files.write(Paths.get("store/receivedFile"), receivePacket.getData());
			System.out.println("Done, file written to the store directory");
		} else {
			System.out.println("Data validation failed.");
			System.out.println("Exiting...");
		}

		serverSocket.close();
	}

}
