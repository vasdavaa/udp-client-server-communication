
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Client {

	public static void main(String[] args) throws Exception {
		DatagramSocket clientSocket = new DatagramSocket();

		// The ip address is important, in this example it is "localhost"
		InetAddress IPAddress = InetAddress.getByName("localhost");

		// Getting the files from the "files" directory
		List<String> files = CommonUtilities.getFilesFromStorage("files");
		System.out.println("Files in the local \"files\" directory: " + files);

		// Getting the user input for the file
		Scanner scan = new Scanner(System.in);
		String userInput = userInputFromArray(scan, "Please select a file by writing it's name: ",
				files.stream().toArray(String[]::new));

		// Creating the byteArray from the file.
		byte[] fileData = Files.readAllBytes(Paths.get("files/" + userInput));

		// Create an array like this: [file bytes], 0, 0, 0...
		byte[] dataContainer = new byte[65500];
		System.arraycopy(fileData, 0, dataContainer, 0, fileData.length);

		// Calculating a sum for the byte array
		int checksum = CommonUtilities.getSumOfByteArray(fileData);

		// Convert it into bytes
		byte[] checksumBytes = ByteBuffer.allocate(4).putInt(checksum).array();

		// Merge arrays
		byte[] mergedDataBytes = concatenateByteArray(dataContainer, checksumBytes);

		DatagramPacket sendPacket = new DatagramPacket(mergedDataBytes,
				mergedDataBytes.length, IPAddress, 8080);
		clientSocket.send(sendPacket);

		scan.close();
		clientSocket.close();
	}

	/**
	 * Merge two byte arrays into one.
	 * 
	 * @param firstArray
	 * @param secondArray
	 * @return merged byte array
	 */
	static byte[] concatenateByteArray(byte[] firstArray, byte[] secondArray) {
		byte[] concatenatedArray = new byte[firstArray.length + secondArray.length];
		System.arraycopy(firstArray, 0, concatenatedArray, 0, firstArray.length);
		System.arraycopy(secondArray, 0, concatenatedArray, firstArray.length, secondArray.length);
		return concatenatedArray;
	}

	/**
	 * Creates a regular expression pattern based on the given text(s).
	 * 
	 * @param texts, is the given text(s) which will be used to create the regex.
	 * @return created regex.
	 */
	static String createPattern(String... texts) {
		return Arrays.asList(texts).stream().map(text -> ("^" + text + "$"))
				.collect(Collectors.joining("|"));
	}

	/**
	 * Verified scan from user. It only accepts input which the pattern allows.
	 * 
	 * @param userInput scanner which will be used for the input.
	 * @param message   display the user a simple message about the input.
	 * @param texts     which are the only acceptable options for the user.
	 * @return Verified input from the user.
	 */
	static String userInputFromArray(Scanner userInput, String message, String... texts) {
		Pattern patterWithAcceptedTexts = Pattern.compile(createPattern(texts));
		Matcher matcher;
		String validatedText = null;
		do {
			System.out.print(message);
			matcher = patterWithAcceptedTexts.matcher(validatedText = userInput.nextLine());
		} while (!(matcher.find()));

		return validatedText;
	}
}
